import java.util.*;

/**
 * Created by tan on 2016-10-20.
 */
public class LambdaDemo {
    public static void main(String[] args) {
//        IStringlength is = new IStringlength() {
//            @Override
//            public int lenth(String param) {
//                return 0;
//            }
//        };
        /// implement interface


        IStringlength is=param -> param.length();
        System.out.println(is.lenth("123123"));
        // random number

        IRandomNumber ir =() -> {
            Random r = new Random();
            return r.nextInt(2);
        };
        System.out.println(ir.random());

        // length of all

        ILength<String,Integer> itl =param -> param.length();
        System.out.println(itl.length("asdsad"));

        //
        List<String> list = Arrays.asList("1","2","3");
        list.forEach(val -> System.out.println(val));
        System.out.println("----------");
        Collections.sort(list);
        list.forEach(val -> System.out.println(val));
        System.out.println("Using lambda");
        list =getList();
        Collections.sort(list,(str1,str2)-> str1.length()-str2.length());
        list.forEach(s -> System.out.println(s));
        // display kieu 2

        list.forEach(System.out::println);
        /// sort
       // Collections.sort(list::sorting);

        System.out.println();

    }
    public static  int sorting(String str1 ,String str2){
        return str1.length()-str2.length();
    }
    private static List<String> getList(){
        return Arrays.asList("1","2","3","4","5","6","a");
    }

}


 interface IStringlength{
    int lenth(String param);
}
interface  IRandomNumber{
    int random();
}
interface ILength<R,P>{
    P length(R param);
}
