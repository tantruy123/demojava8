package com.strategy;

/**
 * Created by tan on 2016-10-20.
 */
public interface Human {
    void doSomeThing(String desc);
}
class LazyPerson implements Human{
    @Override
    public void doSomeThing(String desc) {
        System.out.println("Sleeping........");
    }
}
class HardWorkPerson implements Human{
    @Override
    public void doSomeThing(String desc) {
        System.out.println("5cm/s");
    }
}
class HumanDemo{
    public static void main(String[] args) {
        // waht if we use lambda
        Human lazyPerson =desc -> {
            System.out.println("Sleeping........");
        };
        Human harwordPerson =desc -> {
            System.out.println("5cm/s");
        };
        lazyPerson.doSomeThing("tan");
        harwordPerson.doSomeThing("tan2");
    }
}
