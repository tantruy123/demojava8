package Collections;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tan on 2016-10-20.
 */
public class ListDemo {


    public static void main(String[] args) {
        List<String> list = ProduceValue.get();
        list.forEach(ListDemo::printLength);
        System.out.println("----");
        // predicate integ face
        list.stream().filter(ListDemo::isNotNumber
        ).collect(Collectors.toList());
        list.forEach(ListDemo::printLength);
        //

     Iterator<String> iterator= list.stream().map(s -> s.toUpperCase()).iterator();
        iterator.forEachRemaining(System.out::println);
    }

    private static boolean isNotNumber(String s) {

        try {
            Integer.parseInt(s);
            return false;
        } catch (Exception e) {
            return true;
        }


    }

    static void printLength(String param) {
        System.out.println(param + " : " + param.length());
    }

}
