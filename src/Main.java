import java.util.function.IntFunction;

/**
 * Created by tan on 2016-10-20.
 */
public class Main {
    public static void main(String[] args) {
        IntFunction<Integer> x = param -> param+3;
        System.out.println(x);
        System.out.println(x.apply(3));
    }
}
